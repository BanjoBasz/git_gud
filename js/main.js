window.onload = function() {

  var searchbar = document.getElementById("js--searchbar");
  var nameListItems = document.getElementsByClassName('name-list__item');
  var filter = "";

  searchbar.onkeyup = function(event){
    search(this.value);
  }
  function search(filter){
    for(var i = 0; i < nameListItems.length; i++){
      if(nameListItems[i].innerHTML.indexOf(filter) !== -1){
        nameListItems[i].style.display = "";
      }
      else{
        nameListItems[i].style.display = "none";
      }
    }
  }
}
